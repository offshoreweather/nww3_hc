The sources is stored in Git_Bitbucket

The input file for 201401 is 2.6Gbyte zipped. and takes 50min to download.

The nww3 201401  partition file is 2.6Gb zip and becomes 9.6Gb
it takes 8 minutes to unzip.
To extract Papau.txt for one month took ~30min
 05:22:25 UTC 2014
 05:51:00 UTC 2014
./nww3_partition Papua.txt -11.1 -7.99 139.99 148.001 multi_1.partition.glo_30m.201401


To extact a point from the Papau.txt file was instantaneouse
./nww3_partition Point.txt -9.51 -9.499 146.99 147.001 Papua.txt



The 200502 file is every 3 hours
The 201401 file is hourly!!

-------------

What I've found out so far:

The partitioned file is based on Hason and ???  paper 2001 

The Spectral spread is based on Kuik 1988 (page 71 of 4.18 Manaul)

Not sure if winds are in m/s or 
if the direction is from North clockwise or East Anticlockwise

Data format:
WAVEWATCH III PARTITIONED DATA FILE III  1.01
 yyyymmdd hhmmss     lat     lon   name       nprt depth uabs  udir cabs  cdir
        hs     tp     lp       theta     sp      wf

From P72 of Manual:
1) PHS Wave heights Hs of partitions of the spectrum (see below).
2) PTP Peak (relative) periods of partitions of the spectrum (parabolic
fit).
3) PLP Peak wave lengths of partitions of the spectrum (from peak
period).
4) PSP Mean direction of partitions of the spectrum.
5) Directional spread of partition of the spectrum Cf. Eq. (2.181).
6) PWS Wind sea fraction of partition of the spectrum (see below).
7) TWS Wind sea fraction of the entire spectrum.
8) PNR Number of partitions found in the spectrum.

PWS Partitionned Wind sea fraction.A

---- Sample data for 28 Feb 2005 ---------------------------------------

 20050228 000000  -9.500 147.000  'grid_point'  3 1132.9  5.8 -49.6 0.00   0.0
  0    0.80    7.90   97.34   201.07    61.88   0.33
  1    0.47    3.11   15.07   304.27    28.36   0.96
  2    0.64    7.90   97.34   171.25    29.27   0.00
  3    0.13   11.42  203.54   136.03     8.02   0.00 

i
