  Program nww3_partition
! This program reads nww3  Partitioned data and limits the
! areal extent of the file.
! It is design to extract an area EG Australia and then can be
! used again to obtain a single or a few points.
! Therefore the input and output MUST BE similar
!
! The output is the first argument so that multiple input files(months) can
! be read as they are listed as the last arguments. Currently only 
! the first file is processed. 
! The output file has to be NEW so if the user puts the input file first
!    it will NOT be overwritten.
!
! Usage:
!   nww3_partition outfile slat nlat wlon elon inputfile/s
!
! Author: DS Duncalf 23 Jun 2010
! Last updated DS Duncalf 5 Aug 2014
! (c) Dave Duncalf
!  OWS and RPS can modify and use as required.
! ------------------------------------------------------------------
    IMPLICIT NONE
!
    REAL :: Area = 0.
    INTEGER :: Num_Months = 12
    REAL, PARAMETER :: Pi = 3.1415927
    integer :: ierr,icnt = 0
    character*132 aline,outfile,infile
    character*14 dtype
    integer  yyyymmdd,hh,nspct,i,n
    real lat,lon,dpth,ws,wd
    real hs,tp,lp,theta,sp,wf
    logical X,Y
    real nlat,slat,elon,wlon
!
 integer::narg,cptArg !#of arg & counter of arg
 character(len=132)::argname !Arg name
!
!Check if any arguments are found
 narg=command_argument_count()
!Loop over the arguments
 if(narg.ge.6 )then
!loop across options
   call get_command_argument(1,argname)
   read (argname,'(a)') outfile
   call get_command_argument(2,argname)
   read (argname,*) slat
   call get_command_argument(3,argname)
   read (argname,*) nlat
   call get_command_argument(4,argname)
   read (argname,*) wlon
   call get_command_argument(5,argname)
   read (argname,*) elon
   call get_command_argument(6,argname)
   read (argname,'(a)') infile
else 
  write(*,*) ' There must be 6 of more arguments'
  write(*,*) ' nww3_partition outfile slat nlat wlon elon inputfile/s'
  write(*,*) ' nww3_partition results.out ' , &
             ' -9.5111 -9.4999  146.999 147.001 ' , &
             ' 28Feb2005.txt'
  stop
 end if
!
!   Open(28,file='28Feb2005.txt',form='formatted',err=450, &
    Open(28,file=adjustl(infile), &
         form='formatted',err=450, &
      iostat=ierr)
    if (ierr.ne.0) goto 450
    read(28,'(a)') aline
    open(13,file=adjustl(outfile),status='new',iostat=ierr)
    write(13,'(a)') aline(1:lnblnk(aline))
    read(28,'(a)') aline
    write(13,'(a)') aline(1:lnblnk(aline))
    read(28,'(a)') aline
    write(13,'(a)') aline(1:lnblnk(aline))
    do while (.true.)
      icnt = icnt + 1
      read(28,'(a)') aline
      if(icnt .eq. icnt/100000*100000) write(*,'(i9,a)') icnt, aline(1:60)
!20050228 000000 -76.500 167.000  'grid_point'  3  700.2  4.1 142.4 0.00   0.0 
      read(aline,19) yyyymmdd,hh,lat,lon,dtype,nspct,dpth,ws,wd
19    format(1x,i8,1x,i6,2f8.3,a14,i3,f7.1,2f6.1,f5.2,f6.1)
!     write(*,*)yyyymmdd,hh
      y = .false.
      X = .false.
      if (lat .lt. nlat .and. lat .gt. slat) Y=.true.
      if (lon .lt. elon .and. lon .gt. wlon) X=.true.
!      if (X .and. Y) write(*,'(a)') aline
      if (X .and. Y) write(13,'(a)') aline(1:lnblnk(aline))
!       hs     tp     lp       theta     sp      wf 
!  0    0.89    7.25   82.05   158.08    64.03   0.67
!  1    0.73    3.72   21.65   179.77    23.77   0.99
!  2    0.44    7.25   81.97    25.73     7.97   0.00

      do i=0,nspct
        read(28,'(a)') aline
        read(aline,29)n,hs,tp,lp,theta,sp,wf  
!        if (X .and. Y) write(*,'(a)') aline
        if (X .and. Y) write(13,29) n,hs,tp,lp,theta,sp,wf
        if (ierr.ne.0) goto 550
29  format(i4,6f8.2)
      enddo
    end do
    close(28)
    close(13)
    stop
! ERRORS
450 write(0,'(2a)') 'Error detected. Input file: ',adjustl(infile)
    write(0,*) ' Iostat =',ierr
    stop
550 write(0,'(2a)')  'Error detected. Output  Iostat =',adjustl(outfile)
    write(0,'(a)') ' The OUTPUT FILE MUST NOT EXIST'
    write(0,*)  ' Iostat =',ierr
    stop
END PROGRAM
